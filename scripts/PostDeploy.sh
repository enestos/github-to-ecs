#!/bin/bash
HOME=/home/ubuntu
APP=$HOME/mlmcloud
LOG=$HOME/deploy.log
/bin/echo "$(date '+%Y-%m-%d %X'): ** Before Install Hook Started **" >> $LOG

# Do some actions after deploy.

/bin/echo "$(date '+%Y-%m-%d %X'): ** Before Install Hook Completed **" >> $LOG
