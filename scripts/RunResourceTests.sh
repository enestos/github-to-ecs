#!/bin/bash
HOME=/home/ubuntu
APP=$HOME/mlmcloud
LOG=$HOME/deploy.log
/bin/echo "$(date '+%Y-%m-%d %X'): ** Before Install Hook Started **" >> $LOG

# Run some resource tests.

/bin/echo "$(date '+%Y-%m-%d %X'): ** Before Install Hook Completed **" >> $LOG
